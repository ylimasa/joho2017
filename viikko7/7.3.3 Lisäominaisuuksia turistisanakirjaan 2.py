# TIE-02100 Johdatus ohjelmointiin

def main():
    english_spanish = {"hey": "hola", "thanks": "gracias", "home": "casa"}
    print("Dictionary contents:")
    print(", ".join(sorted(english_spanish.keys())))
    while True:
        command = input("[W]ord/[A]dd/[R]emove/[P]rint/[T]ranslate/[Q]uit: ")

        if command == "W":

            word = input("Enter the word to be translated: ")
            if word in english_spanish:
                print(word, "in Spanish is", english_spanish[word])
            else:
                print("The word", word, "could not be found from the "
                                        "dictionary.")

        elif command == "A":
            key = input("Give the word to be added in English: ")
            value = input("Give the word to be added in Spanish: ")
            english_spanish[key] = value
            print("Dictionary contents:")
            print(", ".join(sorted(english_spanish.keys())))

        elif command == "R":
            key = input("Give the word to be removed: ")
            if key in english_spanish:
                del english_spanish[key]
            else:
                print("The word {} could not be found from the dictionary."
                      .format(key))

        elif command == "P":
            print()
            print("English-Spanish")
            for key, value in sorted(english_spanish.items()):
                print(key, value)

            print()
            print("Spanish-English")
            spanish_english = createSpanishEnglistDictionary(english_spanish)
            for key, value in sorted(spanish_english.items()):
                print(key,value)
            print()

        elif command == "T":
            sentence = str(input("Enter the text to be translated in Spanish: "))
            print("The text, translated by the dictionary:")
            words = sentence.split(" ")
            translation = []
            for word in words:
                if word in english_spanish:
                    translation.append(english_spanish[word])
                else:
                    translation.append(word)
            print(" ".join(translation))

        elif command == "Q":
            print("Adios!")
            return

        else:
            print("Unknown command, enter W, A, R, P, T or Q!")


def createSpanishEnglistDictionary(english_spanish):
    spanish_english = {}
    for key, value in english_spanish.items():
        spanish_english[value] = key
    return spanish_english


main()

