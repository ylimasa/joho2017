def main():
    print("Enter the blacklist. Quit by entering an empty row.")
    blackList = readInput(True)
    # Values are read to blackList as lowercase
    print("Enter text rows to the message. Quit by entering an empty row.")
    textRows = readInput()
    cencorredTextRows = []

    for row in textRows:
        cencorredRow = []
        words = str(row).split(" ")
        for word in words:
            if word.lower() in blackList:
                cencorredRow.append("###")
            else:
                cencorredRow.append(word)
        cencorredTextRows.append(" ".join(cencorredRow))
    print("Censored message:")
    print("\n".join(cencorredTextRows))


def readInput(toLower = False):
    line = None
    array = []
    while line != "":
        if line is not None:
            if toLower == True:
                array.append(line.lower())
            else:
                array.append(line)
        line = str(input())
    return array

main()
