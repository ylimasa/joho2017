
def järjestä_kirjainkoosta_välittämättä(kulkupelejä):
    return sorted(kulkupelejä, key=lambda x: str(x).lower())

