

def main():
    print("Enter rows of text for word counting. Empty row to quit.")
    line = None
    wordList = []
    while True:
        line = str(input()).lower()
        if line == "":
            break
        wordList += line.split(" ")
    dictionary = {}
    while len(wordList) > 0:
        word = wordList[0]
        dictionary[word] = wordList.count(word)
        while word in wordList:
            wordList.remove(word)
    for key, value in sorted(dictionary.items()):
        print("{0} : {1} times".format(key, value))


main()
