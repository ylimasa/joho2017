# TIE-02100 Johdatus ohjelmointiin
# Tehtävä: ristinolla, ohjelmakoodipohja
board = [["."] * 3 for _ in range(3)]

def main():
    # TODO luo pelilaudan tietorakenne
    printBoard()
    vuorot = 0  # Pelattujen vuorojen määrä

    # Peli jatkuu kunnes ruudukko on täynnä.
    # 8 vuoron vaihdon jälkeen laudalle on laitettu 9 merkkiä.
    while vuorot < 9:

        # Päivitetään merkki vuoron mukaan
        if vuorot % 2 == 0:
            merkki = "X"
        else:
            merkki = "O"
        vuorot += 1

        while True:
            koordinaatit = input("Player  " + merkki + ", give coordinates: ")
            try:
                x, y = koordinaatit.split(" ")
                x = int(x)
                y = int(y)
                if board[y][x] != ".":
                    raise Exception

                board[y][x] = merkki
                printBoard()

                break

            except ValueError:
                print("Error: enter two integers, separated with spaces.")
            except IndexError:
                print("Error: coordinates must be between 0 and 2.")
            except Exception:
                print("Error: a mark has already been placed on this square.")


def printBoard():
    global board
    for f in range(0, 3):
        print("".join(board[f]))


main()