
def main():
    results = []
    for f in range(5):
        results.append(float(input("Enter the time for performance {}: "
                                   .format(f+1))))
    results.sort()
    del results[4]
    del results[0]
    print("The official competition score is {:.2f} seconds."
          .format(sum(results)/3))

main()
