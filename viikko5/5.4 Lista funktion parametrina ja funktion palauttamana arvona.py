
def input_to_list(inputCount):
    numbers = []
    print("Enter {} numbers:".format(inputCount))
    for f in range(inputCount):
        numbers.append(int(input()))
    return numbers

def main():
    numberCount = int(input("How many numbers do you want to process: "))
    numbers = input_to_list(numberCount)
    numberToBeSearched = int(input("Enter the number to be searched: "))
    if numbers.count(numberToBeSearched) != 0:
        print("{0} shows up {1} times among the numbers you have entered."
              .format(numberToBeSearched, numbers.count(numberToBeSearched)))
    else:
        print("{} is not among the numbers you have entered."
              .format(numberToBeSearched))
main()
