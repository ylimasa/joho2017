
def main():
    positiveNumbers= []
    print("Give 5 numbers:")
    for f in range(5):
        number = int(input("Next number: "))
        if number > 0:
            positiveNumbers.append(number)
    print("The numbers you entered that were greater than zero were:")
    for f in positiveNumbers:
        print(f)

main()
