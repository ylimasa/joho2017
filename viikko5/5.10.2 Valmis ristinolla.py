# TIE-02100 Johdatus ohjelmointiin
# Tehtävä: ristinolla, ohjelmakoodipohja
board = [["."] * 3 for _ in range(3)]

def main():
    # TODO luo pelilaudan tietorakenne
    printBoard()
    vuorot = 0  # Pelattujen vuorojen määrä

    # Peli jatkuu kunnes ruudukko on täynnä.
    # 8 vuoron vaihdon jälkeen laudalle on laitettu 9 merkkiä.
    while vuorot < 9:

        # Päivitetään merkki vuoron mukaan
        if vuorot % 2 == 0:
            merkki = "X"
        else:
            merkki = "O"
        vuorot += 1

        while True:
            koordinaatit = input("Player  " + merkki + ", give coordinates: ")
            try:
                x, y = koordinaatit.split(" ")
                x = int(x)
                y = int(y)
                if board[y][x] != ".":
                    raise Exception

                board[y][x] = merkki
                printBoard()
                winner = getWinner()
                if winner is not None:
                    print("The game ended, the winner is", winner)
                    return

                break

            except ValueError:
                print("Error: enter two integers, separated with spaces.")
            except IndexError:
                print("Error: coordinates must be between 0 and 2.")
            except Exception:
                print("Error: a mark has already been placed on this square.")
    print("Draw!")

def printBoard():
    global board
    for f in range(0, 3):
        print("".join(board[f]))

def getWinner():
    O = ["0"] * 3
    X = ["X"] * 3
    global board
    wins = []
    # Kakkaa mutta toimii
    wins.append(board[0][:3])
    wins.append(board[1][:3])
    wins.append(board[2][:3])

    wins.append([board[0][0],board[1][0],board[2][0]])
    wins.append([board[0][1],board[1][1],board[2][1]])
    wins.append([board[0][2],board[1][2],board[2][2]])

    wins.append([board[0][0],board[1][1],board[2][2]])
    wins.append([board[0][2],board[1][1],board[2][0]])
    if O in wins:
        return "O"
    if X in wins:
        return "X"
    return None

main()