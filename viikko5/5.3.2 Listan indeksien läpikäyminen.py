
def main():
    numbers= []
    print("Give 5 numbers:")
    for f in range(5):
        number = int(input("Next number: "))
        numbers.append(number)
    print("The numbers you entered, in reverse order:")
    numbers.reverse()
    for f in numbers:
        print(f)

main()
