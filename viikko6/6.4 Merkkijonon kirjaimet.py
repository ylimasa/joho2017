
def main():
    vowels = ["i", "y", "u", "e", "ö", "o", "ä", "a"]
    vc = 0
    word = str(input("Enter a word: "))
    for f in word:
        if f in vowels:
            vc += 1
    print("The word {2} contains {0} vowels and {1} consonants".format(vc, len(word)-vc, word))


main()
