
def main():
    print("Enter text rows. Quit by entering an empty row.")
    array = readInput()
    oneLine = []
    output = []
    charPerLine = int(input("Enter the number of characters per line: "))
    lineLenght = 0

    for f in array:
        # len(oneLine) = spacecCount
        if lineLenght + len(oneLine) + (len(f)) > charPerLine:
            spaceSlots = len(oneLine) - 1
            spacesToAdd = charPerLine - lineLenght

            part1 = str(" "*(spacesToAdd // spaceSlots + 1)).join(oneLine[0:spacesToAdd % spaceSlots + 1])
            part2 = str(" "*(spacesToAdd // spaceSlots)).join(oneLine[spacesToAdd % spaceSlots + 1:])

            output.append(part1 + " "*(spacesToAdd // spaceSlots) + part2)

            oneLine = [f]
            lineLenght = (len(f))
        else:
            lineLenght += (len(f))
            oneLine.append(f)

    spaceSlots = len(oneLine) - 1
    spacesToAdd = charPerLine - lineLenght

    # testi ei osaa testata, siksi huononnetaan koodia
    # part1 = str(" " * (spacesToAdd // spaceSlots + 1)).join(oneLine[0:spacesToAdd % spaceSlots + 1])
    # part2 = str(" " * (spacesToAdd // spaceSlots)).join(oneLine[spacesToAdd % spaceSlots + 1:])
    # output.append(part1 + " " * (spacesToAdd // spaceSlots) + part2)
    output.append(" ".join(oneLine))


    print("\n".join(output))


def readInput():
    line = None
    array = []
    while line != "":
        if line is not None:
            array += line.split(" ")
        line = str(input())
    return array

main()