

def capitalize_initial_letters(string):
    if string == "":
        return ""
    strings = string.split(" ")
    array = []
    for f in strings:
        array.append(f[0].upper()+f[1:].lower())
    return " ".join(array)
