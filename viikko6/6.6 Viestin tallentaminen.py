# TIE-02100 Johdatus ohjelmointiin
# TIE-02106 Introduction to Programming
# Task: viesti, program code template


def main():
    print("Enter text rows to the message. Quit by entering an empty row.")
    msg = read_message()

    print("The same, shouting:")
    print("\n".join(msg).upper())


def read_message():
    list = []
    row = None
    while row != "":
        if row is not None:
            list.append(row)
        row = str(input())
    return list

main()
