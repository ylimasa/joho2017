# TIE-02100 Johdatus ohjelmointiin
# Tehtävä: ROT-13, ohjelmakoodipohja


def encrypt(merkki):
    upper = False
    SELKOMERKIT = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l",
                        "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w",
                        "x", "y", "z"]

    SALAMERKIT = ["n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y",
                            "z", "a", "b", "c", "d", "e", "f", "g", "h", "i",
                            "j", "k", "l", "m"]

    # Täydennä merkin salaaminen tähän
    if str(merkki).isupper():
        merkki = str(merkki).lower()
        upper = True
    if merkki in SELKOMERKIT:
        index = SELKOMERKIT.index(merkki)
        merkki = SALAMERKIT[index]
    if upper:
        merkki = str(merkki).upper()

    return merkki


def row_encryption(string):
    encryptedString = ""
    for f in string:
        encryptedString += encrypt(f)
    return encryptedString


def lue_viesti():
    line = None
    message = []
    while line != "":
        if line is not None:
            message.append(line)
        line = str(input())
    return message


def main():
    print("Syötä viestin tekstirivejä. Lopeta syöttämällä tyhjä rivi.")
    viesti = lue_viesti()

    print("ROT13:")
    for f in viesti:
        print(row_encryption(f))

main()
