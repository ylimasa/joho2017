
def reverse_name(name):
    if len(name) <= 1:
        return ""
    name = str(name).replace(" ",",")
    array = str(name).split(",")
    array2=[]
    for f in array:
        if len(f) > 0:
            array2.append(f)
    array2.insert(0, array2[-1])
    del array2[-1]
    name2 = " ".join(array2)

    return name2

print(reverse_name("Teekkari, Teemu"))
print(reverse_name("Simakuutio ,    Ahto"))
print(reverse_name("Onnenkantamoinen,Oskari"))
print(reverse_name("von Grünbaumberger, Herbert"))
print(reverse_name(","))
print(reverse_name(""))