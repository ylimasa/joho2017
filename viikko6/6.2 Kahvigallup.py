
def main():
    print ("Enter one response per line. End by entering an empty row.")
    drinkers, notDrinkingCount = readValues()
    if notDrinkingCount != 0:
        print("Removed {} non-coffee-drinkers responses"
              .format(notDrinkingCount))

    if len(drinkers) == 0:
        return

    mostCommonValue = printStatistics(drinkers)

    print()
    print("The greatest response: {} cups of coffee per day"
          .format(max(drinkers)))
    print("The most common response: {} cups of coffee per day"
          .format(mostCommonValue))

    # +/- 1 mostCommonValue
    percentualPart1 = ((drinkers.count(mostCommonValue-1) +
                      drinkers.count(mostCommonValue) +
                      drinkers.count(mostCommonValue+1)) / len(drinkers)) * 100

    print("{0:.1f}% of the respondents drink {1}-{2} cups of coffee per day"
        .format(percentualPart1, mostCommonValue-1, mostCommonValue+1))
    print()

    if max(drinkers) < 5:
        # if there is no coffee abusers
        return

    print("Information related to coffee abusers:")

    # How many percents drink more than 4 cups
    percentualPart2 = ((len(drinkers) -
                       drinkers.count(1) -
                       drinkers.count(2) -
                       drinkers.count(3) -
                       drinkers.count(4)) / len(drinkers)) * 100


    print("{0:.1f}% of the respondents drink more than 4 cups of "
          "coffee per day".format(percentualPart2))

    litleTooMuchCount = drinkers.count(5) + drinkers.count(6) + \
                        drinkers.count(7) + drinkers.count(8)

    print("{0} respondents drink a little too much coffee (5-8 cups per day)"
          .format(litleTooMuchCount))


    doubleTooMuch(drinkers)
    tooMuchDrinkerCount = 0
    tooMuchDrinkers = []
    for f in drinkers:
        if f > 8:
            tooMuchDrinkers.append(f)
            tooMuchDrinkerCount += 1
    print("{} respondents drink over double the recommendation"
          .format(tooMuchDrinkerCount))

    print("The responses over 8 cups of coffee per day:")
    for f in tooMuchDrinkers:
        print(f)


def doubleTooMuch(drinkers):
    count = 0
    if max(drinkers) > 8:
        for f in range(8,max(drinkers) + 1):
            count += drinkers.count(f)
    return count


def printStatistics(drinkers):
    print()
    print("Information related to coffee drinkers:")
    mostCommonValue = 0
    mostCommonValueCount = 0
    for f in range(min(drinkers),max(drinkers)+1):
        print("{0:2} {1}".format(f,"#"*drinkers.count(f)))
        if drinkers.count(f) > mostCommonValueCount:
            mostCommonValueCount = drinkers.count(f)
            mostCommonValue = f
    return mostCommonValue


def readValues():
    drinker = None
    drinkers = []
    notDrinkingCount = 0
    while drinker != "":
        if drinker is not None:
            if int(drinker) == 0:
                notDrinkingCount += 1
            else:
                drinkers.append(int(drinker))
        drinker = str(input())
    return drinkers, notDrinkingCount

main()
