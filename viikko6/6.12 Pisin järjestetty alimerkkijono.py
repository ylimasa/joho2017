
def longest_substring_in_order(string):
    ALPHABET = "abcdefghijklmnopqrstuvwxyzåäö"
    longestSubstring = ""
    subString = ""
    prevIndex = -1
    for f in string:
        if f in ALPHABET:
            if ALPHABET.index(f) > prevIndex:
                prevIndex = ALPHABET.index(f)
                subString += f
                if len(subString) > len(longestSubstring):
                    longestSubstring = subString
            else:
                subString = f
                prevIndex = ALPHABET.index(f)

    return longestSubstring

