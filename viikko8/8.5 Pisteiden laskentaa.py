
def main():
    filename = str(input("Enter the name of the score file: "))
    print("Contestant score:")
    file = open(filename, "r")
    lines = file.readlines()
    scores = {}
    for line in lines:
        array = line.rstrip().split(" ")
        if array[0] in scores:
            scores[array[0]] += int(array[1])
        else:
            scores[array[0]] = int(array[1])
    for f in sorted(scores.keys()):
        print(f, scores[f])

main()