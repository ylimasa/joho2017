
def main():
    fileName = str(input("Enter the name of the file: "))
    try:
        file = open(fileName, "r")
    except:
        print("There was an error in reading the file.")
        return
    lines=file.readlines()
    x = 0
    for line in lines:
        x+=1
        print(x, line, end="")

main()