# Johdatus ohjelmointiin
# Print a box with input checking


def main():

    width = read_input("Enter the width of a frame: ")
    height = read_input("Enter the height of a frame: ")
    mark = str(input("Enter a print mark: "))

    print_box(width, height, mark)


def read_input(text):
    while True:
        try:
            value = int(input(text))
            if value >= 1:
                break
        except ValueError:
            ()
    return value


def print_box(w, h, m):
    print()
    for height in range(0, h):
        print(m*w)

main()

