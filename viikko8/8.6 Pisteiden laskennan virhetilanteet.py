
def main():
    filename = str(input("Enter the name of the score file: "))
    try:
        file = open(filename, "r")
    except:
        print("There was an error in reading the file.")
        return
    lines = file.readlines()
    scores = {}
    for line in lines:
        if line.count(" ") != 1:
            print("There was an erroneous line in the file: ")
            print(line)
            return
        array = line.rstrip().split(" ")
        try:
            if array[0] in scores:
                scores[array[0]] += int(array[1])
            else:
                scores[array[0]] = int(array[1])
        except:
            print("There was an erroneous score in the file: ")
            print(array[1])
            return
    print("Contestant score:")
    for f in sorted(scores.keys()):
        print(f, scores[f])

main()