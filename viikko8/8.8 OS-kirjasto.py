import os

def fix_filenames(folder):
    list1 = os.listdir(folder)
    list2 = []
    for f in list1:
        if str(f[-4:]).lower() == ".mp3":
            list2.append(f)

    for file in list2:
        if file.count("-") == 2 and file[2] == "-":
            line = str(file)[0:-4].split("-")
            file2 = line[2] + "-" + line[1] + ".mp3"
            file = os.path.join(folder, file)
            file2 = os.path.join(folder, file2)
            os.rename(file, file2)

