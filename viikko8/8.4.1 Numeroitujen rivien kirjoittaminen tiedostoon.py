
def main():
    filename = str(input("Enter the name of the file: "))
    try:
        file = open(filename, "w")
    except:
        print("Writing the file {0} was not successful.".format(filename))
        return
    print("Enter rows of text. Quit by entering an empty row.")
    line = None
    lines = []
    while line !="":
        if line is not None:
            lines.append(line)
        line = str(input())
    try:
        x = 0
        for line in lines:
            x += 1
            file.write(str(x)+" "+line+"\n")
        print("File {0} has been written.".format(filename))
    except:
        print("Writing the file {0} was not successful.".format(filename))

main()