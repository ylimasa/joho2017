# Introduction to Programming K2017

###############################################################################
# read_biometric_registry(filename)
# =========================================
# Function reads the biometric information from the file whose name is in
# the parameter filename. The read information will be parsed and saved in
# the data structure that is in the variable called result. The coder has
# to define the data structure by him/herself.
# After successfully reading the file and saving its contents in the data
# structure, the function returns the result. If there's an error, None is
# returned.
#
# PLEASE NOTE:
# (a) Implement all parts of the code that say TODO.
# (b) The data structure returned by the function must be something that
#     that nests lists and/or dicts. That is the wole point of this project:
#     to use nested data structures.
###############################################################################

from math import sqrt


def read_biometric_registry(filename):
    # result = TODO # initialize your data structure here
    result = []
    result2 = {}
    index = 0
    handled_passports = []

    try:
        with open(filename, "r") as file_object:
            for row in file_object:
                row = row.rstrip()

                fields = row.split(";")

                if len(fields) != 8:
                    print(
                        "Error: there is a wrong number of fields in the file:")
                    print("'", row, "'", sep="")
                    return None

                for ind in range(3, 8):
                    fields[ind] = float(fields[ind])
                    if not (0 <= fields[ind] <= 3.0):
                        print("Error: there is a erroneous value in the file:")
                        print("'", row, "'", sep="")
                        return None

                name = fields[0] + ", " + fields[1]
                passport = fields[2]
                biometric = fields[3:]

                if passport in handled_passports:
                    print("Error: passport number", passport,
                          "found multiple times.")
                    return None

                else:
                    handled_passports.append(passport)

                    # TODO:
                    # save the read information in the result data structure
                    #   result.append("{0};{1};{2}".format(
                    #      name, passport, biometric))
                    result2[index] = {}
                    result2[index]["passport"] = passport
                    result2[index]["name"] = name
                    result2[index]["biometrics"] = biometric
                    index += 1

                    result.append(result2)

                    # print(biometric)
                    # print(result[passport]["biometrics"])

        return result

    except FileNotFoundError:
        print("Error: file", filename, "could not be opened.")

    except ValueError:
        print("Error: there's a non-numeric value on row:")
        print("'", row, "'", sep="")

    return None


###############################################################################
# TODO
###############################################################################
def execute_match(registry):
    # TODO
    checkedPersons = []

    itemcount = len(registry)
    for item in range(0, itemcount):
        samePersons = []

        for item2 in range(0, itemcount):
            if not item == item2 and item2 not in checkedPersons:
                regValues = registry[item][item]["biometrics"]
                regValues2 = registry[item2][item2]["biometrics"]
                biometricValue = sqrt(

                    (float(regValues2[0]) - float(regValues[0])) ** 2 +
                    (float(regValues2[1]) - float(regValues[1])) ** 2 +
                    (float(regValues2[2]) - float(regValues[2])) ** 2 +
                    (float(regValues2[3]) - float(regValues[3])) ** 2 +
                    (float(regValues2[4]) - float(regValues[4])) ** 2
                )
                if biometricValue < 0.1:

                    if item not in samePersons:
                        samePersons.append(item)

                    samePersons.append(item2)
        checkedPersons += samePersons

        if len(samePersons) > 0:
            print("Probably the same person:")
            for person in samePersons:
                print("{0};{1};{2:.2f};{3:.2f};{4:.2f};{5:.2f};{6:.2f}".format(
                    registry[person][person]["name"],
                    registry[person][person]["passport"],
                    registry[person][person]["biometrics"][0],
                    registry[person][person]["biometrics"][1],
                    registry[person][person]["biometrics"][2],
                    registry[person][person]["biometrics"][3],
                    registry[person][person]["biometrics"][4]
                ))
            print()
    if len(checkedPersons) == 0:
        print("No matching persons were found.")


###############################################################################
# TODO
###############################################################################
def execute_search(registry):
    # TODO
    while True:
        search = str(
            input("enter 5 measurement points separated by semicolon: "))
        searchValues = search.split(";")

        try:
            if len(searchValues) != 5:
                print("Error: wrong number of measurements. Try again.")
            elif not all(isinstance(float(x), float) for x in searchValues):
                print("Error: enter floats only. Try again.")
            else:
                break
        except ValueError:
            print("Error: enter floats only. Try again.")

    atLeastOneFound = False
    itemcount = len(registry)
    for item in range(0, itemcount):

        regValues = registry[item][item]["biometrics"]

        biometricValue = sqrt(
            (float(searchValues[0]) - float(regValues[0])) ** 2 +
            (float(searchValues[1]) - float(regValues[1])) ** 2 +
            (float(searchValues[2]) - float(regValues[2])) ** 2 +
            (float(searchValues[3]) - float(regValues[3])) ** 2 +
            (float(searchValues[4]) - float(regValues[4])) ** 2
        )
        if biometricValue < 0.1:
            if atLeastOneFound == False:
                atLeastOneFound = True
                print("Suspects found:")
                print("{0};{1};{2:.2f};{3:.2f};{4:.2f};{5:.2f};{6:.2f}".format(
                registry[item][item]["name"],
                registry[item][item]["passport"],
                registry[item][item]["biometrics"][0],
                registry[item][item]["biometrics"][1],
                registry[item][item]["biometrics"][2],
                registry[item][item]["biometrics"][3],
                registry[item][item]["biometrics"][4]
            ))

    if atLeastOneFound == False:
        print("No suspects were found.")
    print()


###############################################################################
# command_line_user_interface
# Very simple user interface. It might be good to add some helper functions.
#
###############################################################################
def command_line_user_interface(registry):
    while True:
        command = input("command [search/match/<enter>] ")
        if command == "":
            return
        elif command == "match":
            execute_match(registry)
        elif command == "search":
            execute_search(registry)
        else:
            print("Error: unknown command '", command,
                  "': try again.", sep="")


###############################################################################
# main()                                                                      #
# ======                                                                      #
# Main program for the project. You're not supposed to edit this.
#
###############################################################################
def main():
    registry_file = input("Enter the name of the registry file: ")

    biometric_registry = read_biometric_registry(registry_file)
    if biometric_registry is not None:
        command_line_user_interface(biometric_registry)


main()
