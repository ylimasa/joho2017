# TIE-02100 Johdatus ohjelmointiin
# Dancing Queen

def read_file(filename):
    # reads the played songs and their scores from the file

    try:
        fileobject = open(filename, "r")

        # TODO: initialize your data structure here
        dict = {}

        # loop over fileobject row by row
        for row in fileobject:
            parts = row.strip().split(";")
            player = parts[0]  # name of the player
            songs = parts[1:]  # songs

            # TODO: make a data structure for this single
            #       player that includes his/her songs and scores

            dict[player] = {}

            # loop over this player's songs
            for song in songs:
                parts = song.split(":")
                results = parts[1].split(",")
                name = parts[0]  # name of the song
                # list of presses, all integer
                # results = [int(luku) for luku in tulokset]

                maxPoints = 0
                for f in results:
                    maxPoints += float(f)*5
                points = float(results[0])*5 + \
                         float(results[1])*4+\
                         float(results[2])*2+\
                         float(results[3])*0+\
                         float(results[4])*(-6)+\
                         float(results[5])*(-12)


                dict[player][name] = points /maxPoints *100

                # TODO: connect these results to the song

                # TODO: add this player's data structure into the
                #       main data structure

        return  dict# TODO return the main data structure

    except IOError:
        print("Error! The file could not be read.")
        return None


def main():
    coefficients = [5, 4, 2, 0, -6, -12]

    filename = input("Enter the name of the file: ")
    dict = read_file(filename)

    # TODO : print ...

    for player in sorted(dict.keys()):
        print(player + ":")
        for f in sorted(dict[player]):
            print("- {0}: {1:.2f}%".format(f, dict[player][f]))


main()
