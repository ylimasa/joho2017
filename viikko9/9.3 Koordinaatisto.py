
def printBoard(width, height, board):

    for f in range(1, width + 1):
        if f == 1:
            #bug here
            print(" ", f, end="")
        else:
            print(f % 10, end="")

    for f in range(0, width):
        if f != 0:
            print("-", end="")
        else:
            print()
            #bug here
            print(" +-", end="")

    for f in range(1, height + 1):
        if f == 1:
            print()
        print(str(f % 10) + "|" + "".join(board[f - 1]))


def main():
    width = 0
    height = 0
    while not 1 <= width <= 20:
        width = int(input("Enter the width: "))
        if not 1 <= width <= 20:
            print("Erroneous input!")
    while not 1 <= height <= 10:
        height = int(input("Enter the height: "))
        if not 1 <= height <= 10:
            print("Erroneous input!")

    board = [["."] * width for _ in range(height)]

    printBoard(width, height, board)
    while True:
        addPoint = str(input("Shall we add a point (Y/N)? "))

        if addPoint.lower() == "y":
            x = int(input("Enter X-coordinate: "))
            if x > width or x <= 0:
                print("The point is outside the coordinate system.")
            else:
                y = int(input("Enter Y-coordinate: "))
                if y > height or y <= 0:
                    print("The point is outside the coordinate system.")
            if 0 < x <= width and 0 < y <= height:
                board[y-1][x-1] = "X"
                printBoard(width,height,board)

        elif addPoint.lower() == "n":
            return
        else:
            print("Erroneous input!")


main()
