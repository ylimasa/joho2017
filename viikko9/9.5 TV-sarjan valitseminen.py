# TIE-02100 Johdatus ohjelmointiin

def read_file(filename):
    # reads and saves the series and their genres from the file

    # TODO initialize a new data structure
    dict = {}
    availebleGenres = []
    try:
        file = open(filename, "r")

        for row in file:
            name, genres = row.rstrip().split(";")
            genres = genres.split(",")
            for genre in genres:
                if genre not in availebleGenres:
                    availebleGenres.append(genre)
            # TODO add the info to the data structure
            dict[name] = genres

        file.close()
        return dict, availebleGenres# TODO return the data structure

    except ValueError:
        print("Error: rows were not in the format name;genres.")
        return None

    except IOError:
        print("Error: the file could not be read.")
        return None


def main():

    filename = input("Enter the name of the file: ")
    dict = {}
    dict, availebleGenres = read_file(filename)
    # TODO print the genres

    print("Available genres are: " + ", ".join(sorted(availebleGenres)))

    while True:
        genre = input("> ")
        array = []
        if genre == "exit":
            return

        # TODO print the series ...
        for f in dict.keys():
            if genre in dict[f]:
                array.append(f)
        array.sort()
        for f in array:
            print(f)


main()
