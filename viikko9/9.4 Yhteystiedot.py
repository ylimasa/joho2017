
def read_file(fileName):
    file = open(fileName, "r")
    lines = file.readlines()
    dict = {}
    for line in lines:
        line2 = line[0:-1]
        array = line2.split(";")

        dict[array[0]] = {}
        if len(array) >= 2:
            dict[array[0]]["name"] = array[1]
            if len(array) >= 3:
                dict[array[0]]["phone"] = array[2]
                if len(array) >= 4:
                    dict[array[0]]["email"] = array[3]
                    if len(array) >= 5:
                        dict[array[0]]["skype"] = array[4]
    return dict
