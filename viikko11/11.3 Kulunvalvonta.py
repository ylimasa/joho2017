
DOORCODES = {'TC114': ['TIE'], 'TC203': ['TIE'], 'TC210': ['TIE', 'TST'],
             'TD201': ['TST'], 'TE111': [], 'TE113': [], 'TE115': [],
             'TE117': [], 'TE102': ['TIE'], 'TD203': ['TST'], 'TA666': ['X'],
             'TC103': ['TIE', 'OPET', 'SGN'], 'TC205': ['TIE', 'OPET', 'ELT'],
             'TB109': ['OPET', 'TST'], 'TB111': ['OPET', 'TST'],
             'TB103': ['OPET'], 'TB104': ['OPET'], 'TB205': ['G'],
             'SM111': [], 'SM112': [], 'SM113': [], 'SM114': [],
             'S1': ['OPET'], 'S2': ['OPET'], 'S3': ['OPET'], 'S4': ['OPET'],
             'K1705': ['OPET'], 'SB100': ['G'], 'SB202': ['G'],
             'SM220': ['ELT'], 'SM221': ['ELT'], 'SM222': ['ELT'],
             'secret_corridor_from_building_T_to_building_F': ['X', 'Y', 'Z'],
             'TA': ['G'], 'TB': ['G'], 'SA': ['G'], 'KA': ['G']}


class Accesscard:
    def __init__(self, id, name):
        self.id = id
        self.name = name
        self.access_codes = []

    def info(self):
        print("{0}, {1}, access: {2}".format(self.id, self.name,
                                        ", ".join(sorted(self.access_codes))))

    def get_name(self):
        return self.name

    def add_access(self, new_access_code):
        self.access_codes.append(new_access_code)

        for access_code in self.access_codes:  # Removes duplicate access codes
            if access_code in DOORCODES.keys():
                for f in DOORCODES[access_code]:
                    if f in self.access_codes:
                        self.access_codes.remove(access_code)

    def check_access(self, door):
        if door in self.access_codes:
            return True

        for access_code in self.access_codes:
            if access_code in DOORCODES[door]:
                return True

        return False

    def merge(self, card):
        for access_code in card.access_codes:
            if not access_code_exists_on_card(self, access_code):
                self.add_access(access_code)


def main():
    access_cards = []
    try:
        access_info_file = open("accessinfo.txt", "r")
    except:
        print("Error: file cannot be read.")
        return

    for line in access_info_file:
        strings = line.rstrip().split(";")

        if len(strings) == 3:
            new_access_card = Accesscard(strings[0], strings[1])
            access_codes = strings[2].split(",")

            for access_code in access_codes:
                new_access_card.add_access(access_code)

            access_cards.append(new_access_card)

    while True:
        line = input("command> ")

        if line == "":
            break

        strings = line.split()
        command = strings[0]

        if command == "list" and len(strings) == 1:
            for access_card in sorted(access_cards, key=lambda x: x.id):
                access_card.info()

        elif command == "info" and len(strings) == 2:
            card_id = strings[1]
            card_found, card = get_card_by_id(card_id, access_cards)
            if not card_found:
                print("Error: unknown id.")
            else:
                card.info()

        elif command == "access" and len(strings) == 3:
            card_id = strings[1]
            door_id = strings[2]

            card_found, card = get_card_by_id(card_id, access_cards)
            if card_found:
                if door_id in DOORCODES.keys():
                    if card.check_access(door_id):
                        print("Card {0} ( {1} ) has access to door {2}".format(
                            card.id, card.name, door_id))
                    else:
                        print("Card {0} ( {1} ) has no access to door {2}".
                              format(card.id, card.name, door_id))
                else:
                    print("Error: unknown doorcode.")
            else:
                print("Error: unknown id.")

        elif command == "add" and len(strings) == 3:
            card_id = strings[1]
            access_code = strings[2]

            card_found, card = get_card_by_id(card_id, access_cards)
            if card_found:
                if access_code_exists(access_code):
                    if not access_code_exists_on_card(card, access_code):
                        card.add_access(access_code)
                else:
                    print("Error: unknown accesscode.")
            else:
                print("Error: unknown id.")

        elif command == "merge" and len(strings) == 3:
            card_id_to = strings[1]
            card_id_from = strings[2]

            card_to_found, card_to = get_card_by_id(card_id_to, access_cards)
            card_from_found, card_from = get_card_by_id(card_id_from,
                                                        access_cards)

            if not card_to_found or not card_from_found:
                print("Error: unknown id.")
            else:
                card_to.merge(card_from)

        elif command == "quit":
            print("Bye!")
            return
        else:
            print("Error: unknown command.")


def get_card_by_id(id, access_cards):
    for access_card in access_cards:
        if access_card.id == id:
            return True, access_card
    return False, None


def access_code_exists(access_code):
    if access_code in DOORCODES.keys():
        return True
    for f in DOORCODES.values():
        if access_code in f:
            return True
    return False


def access_code_exists_on_card(card, access_code):
    if access_code in card.access_codes:
        return True
    if access_code in DOORCODES.keys():
        for f in DOORCODES[access_code]:
            if f in card.access_codes:
                return True
    return False

main()
