
def reverse_name(name):
    if len(name) <= 1:
        return ""
    name = str(name).replace(" ",",")
    array = str(name).split(",")
    array.insert(0, array[-1])
    name2 = " ".join(array)
    return name2

print(reverse_name("Teekkari, Teemu"))
print(reverse_name("Simakuutio ,    Ahto"))
print(reverse_name("Onnenkantamoinen,Oskari"))
print(reverse_name("von Grünbaumberger, Herbert"))
print(reverse_name(","))
print(reverse_name(""))