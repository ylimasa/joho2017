
string = str("0123456789")

f = 3
print(string[f:f+4])

print(5 % 3)

print(string[f:])

oneLine = ["a","a","f", "d", "3"]
spacesToAdd = 2

part1 = "  ".join(oneLine[0:spacesToAdd])
part2 = " ".join(oneLine[spacesToAdd:])

print(part1+part2)