# Johdatus ohjelmointiin
# Parasetamol

def calculate_dose(weight, time, given):
    if int(time) < 6:
        return 0
    if int(given) >= 4000:
        return 0
    max = 4000-int(given)
    if int(weight)*15 > max:
        return max
    else:
        return int(weight)*15

def main():
    weight = input("Patient's weight (kg): ")
    time = input("How much time has passed from the previous dose "
                 "(full hours): ")
    given = input("The total dose for the last 24 hours (mg): ")

    print("The amount of Parasetamol to give to the patient:",
          calculate_dose(weight, time, given))


main()
