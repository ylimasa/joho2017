

def main():
    measurenments = []
    FIRSTMEASURENMENT = 0
    count = int(input("Enter the number of the measurements: "))
    if count <= 0:
        print("Error: the number must be expressed as a positive integer.")
        return

    for measurement in range(0,count):
        measurenments.append(float(input("Enter the measurement result {0}: "
                              .format(measurement + 1))))

        if not 6 <= measurenments[measurement] <= 8:
            print("The conditions are not suitable for zebra fishes.")
            return

        if not measurement == FIRSTMEASURENMENT:
            if abs(measurenments[measurement] -
                           measurenments[measurement - 1]) > 1:
                print("The conditions are not suitable for zebra fishes.")
                return

    print("Conditions are suitable for zebra fishes. The average pH "
          "is {0:.2f}.".format(sum(measurenments) / count))

main()








