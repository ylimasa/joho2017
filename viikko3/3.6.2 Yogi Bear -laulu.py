# TIE-02100 Johdatus ohjelmointiin
# Koodipohja laulu c, Yogi Bear



def main():
    verse("I know someone you don't know", "Yogi")
    verse("Yogi has a best friend too", "Boo Boo")
    verse("Yogi has a sweet girlfriend", "Cindy")



def repeat_name(name, count):
    for line in range(0,count):
        print("{0}, {0} Bear".format(name))

def verse(blaablaa, name):
    print(blaablaa)
    print("{0}, {0}".format(name))
    print(blaablaa)
    repeat_name(name, 3)
    print(blaablaa)
    repeat_name(name, 1)
    print()


main()