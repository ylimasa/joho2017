def main():

    print_verse("cow", "moo")
    print_verse("pig", "oink")
    print_verse("duck", "quack")
    print_verse("horse", "neigh")
    print_verse("lamb", "baa")

def print_verse(animal, sound):
    print("Old MACDONALD had a farm")
    print("E-I-E-I-O")
    print("And on his farm he had a", animal)
    print("E-I-E-I-O")
    print("With a {0} {0} here".format(sound))
    print("And a {0} {0} there".format(sound))
    print("Here a {0}, there a {0}".format(sound))
    print("Everywhere a {0} {0}".format(sound))
    print("Old MacDonald had a farm")
    print("E-I-E-I-O")
    print()

main()
