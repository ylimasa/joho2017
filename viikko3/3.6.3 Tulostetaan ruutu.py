
# TIE-02100 Johdatus ohjelmointiin
# TIE-02106 Introduction to Programming
# Template for task: ruutu


def main():
    width = int(input("Enter the width for a box: "))
    height = int(input("Enter the height for a box: "))
    mark = str(input("Enter a print mark: "))

    print_box(width, height, mark)


def print_box(w, h, m):
    print()
    for height in range(0, h):
        print(m*w)

main()
