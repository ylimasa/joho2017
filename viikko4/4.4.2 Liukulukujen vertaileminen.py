
def compare_floats(a,b, epsilon):
    return abs(a - b) < epsilon

print(compare_floats(0.0002, 0.0000002, 0.000000001))
