from math import factorial


def main():
    n = int(input("Enter the total number of lottery balls: "))
    p = int(input("Enter the number of the drawn balls: "))
    if int(n) < 0:
        print("The number of balls must be a positive number.")
        return
    if int(p) > int(n):
        print("At most the total number of balls can be drawn.")
        return
    print("The probability of guessing all {0} balls correctly is 1/{1}"
          .format(p, combinations(n, p)))


def combinations(n, p):
    return int(factorial(n) / (factorial(n - p) * factorial(p)))

main()
