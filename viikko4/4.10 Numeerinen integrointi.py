
def parabel(x):
    return -pow(x, 2) + 2*x + 4

def approximate_area(parabel2, min, max, count):
    totalLength = max - min;
    length = totalLength / count
    area = 0
    for f in range(0, int(count / 2)):
        area += abs(parabel2(min + length*f) * length)
    area *= 2
    return area

print(approximate_area(parabel, -1, 3, 4))