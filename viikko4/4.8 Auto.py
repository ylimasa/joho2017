# TIE-02100
# nönnönnöö
# student number


from math import sqrt

# This is a text-based menu. You should ONLY touch TODOs inside the menu.
# TODOs in the menu call functions that you have implemented and take care
# of the return values of the function calls.

def menu():
    tank_size  = read_number("How much does the vehicle's gas tank hold? ")
    gas = tank_size  # Tank is full when we start
    gas_consumption = read_number("How many liters of gas does the car " + \
                                 "consume per hundred kilometers? ")
    x = 0.0  # Current X coordinate of the car
    y = 0.0  # Current Y coordinate of the car

    MENU_TEXT = "1) Fill 2) Drive 3) Quit \n-> "

    while True:
        print("Coordinates x={:.1f}, y={:.1f}, "
              "the tank contains {:.1f} liters of gas.".format(x, y, gas))

        choice = input(MENU_TEXT)

        if choice == "1":
           to_fill = read_number("How many liters of gas to fill up? ")
           gas = fill(tank_size, to_fill, gas)

        elif choice == "2":
           new_x = read_number("x: ")
           new_y = read_number("y: ")
           gas, x, y = drive(x, y, new_x, new_y, gas, gas_consumption)

        elif choice == "3":
           break

    print("Thank you and bye!")


# This function has three parameters which are all FLOATs:
#       (1) the size of the tank
#       (2) the amount of gas that is requested to be filled in
#       (3) the amount of gas in the tank currently
#
# The parameters have to be in this order.
# The function returns one FLOAT that is the amount of gas in the
# tank AFTER the filling up.
#
# The function does not print anything and does not ask for any
# input.
def fill(tankin_koko, tankataan, bensaa_tankissa):
    if (tankin_koko - bensaa_tankissa) >= tankataan:
        return bensaa_tankissa+tankataan
    else:
        return bensaa_tankissa+tankin_koko-bensaa_tankissa

# This function has six parameters. They are all floats.
#   (1) The current x coordinate
#   (2) The current y coordinate
#   (3) The destination x coordinate
#   (4) The destination y coordinate
#   (5) The amount of gas in the tank currently
#   (6) The consumption of gas per 100 km of the car
#
# The parameters have to be in this order.
# The function returns three floats:
#   (1) The amount of gas in the tank AFTER the driving
#   (2) The reached (new) x coordinate
#   (3) The reached (new) y coordinate
#
# The return values have to be in this order.
# The function does not print anything and does not ask for any
# input.
def drive(x,y,x2,y2, bensaa, kulutus_satasella):
    distance = matkan_pituus(x,y,x2,y2)
    maxDistance = kuiPitkälTankilPääsee(kulutus_satasella, bensaa)
    if bensaa == 0 or distance == 0:
        return bensaa,x,y
    if distance <= maxDistance:
        bensaa -= kulutus_satasella/100*distance
        return bensaa,x2,y2
    return 0.0, x + (x2 - x) * maxDistance/distance, y + (y2 - y) * maxDistance/distance

def matkan_pituus(x,y,x2,y2):
    return (sqrt((x2-x)**2+(y2-y)**2))

def kuiPitkälTankilPääsee(kulutus_satasella, bensaa):
    return bensaa / kulutus_satasella * 100


    # Implement your own functions here. It is required to implement at least
    # two functions that take at least one parameter and return at least one
    # value.
    # The functions have to be used somewhere in the program.

def read_number(prompt, error_message="Incorrect input!"):

    # This function reads input from the user.
    # Do not touch this function.
    try:
        return float(input(prompt))
    except ValueError as e:
        print(error_message)
        return read_number(prompt, error_message)


def main():
    menu()

main()
