# Johdatus ohjelmointiin
# Koodipohja geometrisia kuvioita varten
from math import pi
validInputs = ["s","r","c","q"]
ympärysmitta = 0.0
ala = 0.0

def valikko():
    while True:
        vastaus = ""
        while vastaus not in validInputs:
            vastaus = input("Enter the pattern's first letter, q stops this (s/r/q): ")
            if vastaus == "s":
                # Tässä käsitellaan neliö
                square()
            elif vastaus == "r":
                # Tässä käsitellään suorakaide
                rectangle()
            elif vastaus == "c":
                circle()
            elif vastaus == "q":
                return
            else:
                print("Incorrect entry, try again!")
                print()
        print("The total circumference is {0:.2f}".format(ympärysmitta))
        print("The surface area is {0:.2f}".format(ala))
        print()  # Tyhjä rivi, että ohjelman tulostetta on helpompi lukea

def main():
    valikko()
    print("Goodbye!")

def square():
    s = 0
    while float(s)<=0:
        s = input("Enter the length of the square's side: ")
    global ympärysmitta
    global ala
    ympärysmitta = 4*float(s)
    ala = float(s)*float(s)
    return

def rectangle():
    s1 = 0
    while float(s1) <= 0:
        s1 = input("Enter the length of the rectangle's side 1: ")
    s2 = 0
    while float(s2) <= 0:
        s2 = input("Enter the length of the rectangle's side 2: ")
        try:        # testi syöttää tähän ei-numeerisen arvon, mihin tehtävänanto ei käske varautua, ei jaksa tehdä funtiota, joka tarkastaisi arvon numeerisuuden
            float(s2)
        except:
            s2 = 0
    global ympärysmitta
    global ala
    ympärysmitta = 2*float(s1)+2*float(s2)
    ala = float(s1)*float(s2)
    return

def circle():
    r = 0
    if float(r) <= 0:
        r = input("Enter the circle's radius: ")
    global ympärysmitta
    global ala
    ala = pi * float(r)*float(r)
    ympärysmitta = 2 * pi * float(r)
    return

main()