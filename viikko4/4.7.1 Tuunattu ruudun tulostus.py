# Introduction to Programming
# Named parameters

def main():
    print_box(5, 4)
    print_box(3, 8, "*")
    print_box(5, 4, "O", "o")
    print_box(inner_mark=".", border_mark="O", height=4, width=6)

def print_box(width, height, border_mark = "#", inner_mark = " "):
    print(border_mark*width)
    for f in range(0,height-2):
        print(border_mark+inner_mark*(width-2)+border_mark)
    print(border_mark*width)
    print()

main()
