# This dict is a global constant, that will be used to determine the
# effects of the Pokémon attacks. The keys of this dict are Pokémon types.
# The values related to the keys are dicts, where the keys are attack types
# and values are the effect factors of the attacks.
# For example, a Ghost-type attack of a Normal-type Pokémon causes 0.8* harm
# and a Fighting-type attack of the same Pokémon causes 1.25* harm.
TYPES = {"Normal": {"Fighting": 1.25, "Ghost": 0.8},
         "Fighting": {"Flying": 1.25, "Psychic": 1.25, "Fairy": 1.25,
                      "Rock": 0.8, "Bug": 0.8, "Dark": 0.8},
         "Flying": {"Electric": 1.25, "Rock": 1.25, "Ice": 1.25, "Grass": 0.8,
                    "Bug": 0.8, "Fighting": 0.8, "Ground": 0.8},
         "Poison": {"Ground": 1.25, "Psychic": 1.25, "Fighting": 0.8,
                    "Bug": 0.8, "Poison": 0.8, "Grass": 0.8, "Fairy": 0.8},
         "Ground": {"Water": 1.25, "Grass": 1.25, "Ice": 1.25, "Poison": 0.8,
                    "Rock": 0.8, "Electric": 0.8},
         "Rock": {"Fighting": 1.25, "Ground": 1.25, "Steel": 1.25,
                  "Water": 1.25, "Grass": 1.25, "Normal": 0.8, "Flying": 0.8,
                  "Poison": 0.8, "Fire": 0.8},
         "Bug": {"Flying": 1.25, "Rock": 1.25, "Fire": 1.25, "Fighting": 0.8,
                  "Ground": 0.8, "Grass": 0.8},
         "Ghost": {"Ghost": 1.25, "Dark": 1.25, "Bug": 0.8, "Poison": 0.8,
                    "Normal": 0.8, "Fighting": 0.8},
         "Steel": {"Fighting": 1.25, "Ground": 1.25, "Fire": 1.25,
                    "Normal": 0.8, "Flying": 0.8, "Rock": 0.8, "Bug": 0.8,
                    "Steel": 0.8, "Grass": 0.8, "Psychic": 0.8, "Ice": 0.8,
                    "Dragon": 0.8, "Fairy": 0.8, "Poison": 0.8},
         "Fire": {"Ground": 1.25, "Rock": 1.25, "Water": 1.25, "Bug": 0.8,
                   "Steel": 0.8, "Fire": 0.8, "Ice": 0.8, "Fairy": 0.8},
         "Water": {"Grass": 1.25, "Electric": 1.25, "Steel": 0.8, "Fire": 0.8,
                    "Water": 0.8, "Ice": 0.8},
         "Grass": {"Flying": 1.25, "Poison": 1.25, "Bug": 1.25, "Fire": 1.25,
                    "Ice": 1.25, "Ground": 0.8, "Water": 0.8, "Grass": 0.8,
                    "Electric": 0.8},
         "Electric": {"Ground": 1.25, "Flying": 0.8, "Steel": 0.8,
                       "Electric": 0.8},
         "Psychic": {"Bug": 1.25, "Ghost": 1.25, "Dark": 1.25, "Fighting": 0.8,
                      "Psychic": 0.8},
         "Ice": {"Fighting": 1.25, "Rock": 1.25, "Steel": 1.25, "Fire": 1.25,
                  "Ice": 1.25},
         "Dragon": {"Ice": 1.25, "Dragon": 1.25, "Fairy": 1.25, "Fire": 0.8,
                     "Grass": 0.8, "Water": 0.8, "Electric": 0.8},
         "Dark": {"Fighting": 1.25, "Bug": 1.25, "Fairy": 1.25, "Ghost": 0.8,
                   "Psychic": 0.8},
         "Fairy": {"Poison": 1.25, "Steel": 1.25, "Fighting": 0.8, "Bug": 0.8,
                    "Dark": 0.8, "Dragon": 0.8}}

def factor(attack_type, pokemon_type):
    """
    Finds the effectiveness factor of an attack from the above defined
    datastructure.
    :param attack_type: String
    :param pokemon_type: String
    :return: Returns the effectiveness factor of the attack
    """
    if pokemon_type in TYPES:

        if attack_type in TYPES[pokemon_type]:
            return TYPES[pokemon_type][attack_type]

    return 1

class Pokemon:
    """ Implements one Pokémon that has a name, types, hitpoints, level
    and moves."""

    def __init__(self, species, types, hp=50, level=20):
        """
        Constructor of the class. Checks the kesto and level and stores
        the attributes.

        :param species: the species of the pokemon
        :param types:   the types of the pokemon
        :param hp:      the hp of the pokemon
        :param level:   the level of the pokemon
        """

        self.__species = species.capitalize()
        self.__types = types

        if not isinstance(hp, int) or not isinstance(level, int) \
                or hp < 0 or level < 1:
            raise ValueError

        self.__hp = hp
        self.__max_hp = hp
        self.__level = level
        self.__moves = {}

    def info(self):
        """
        Prints information in the form of species, types, hp.
        """
        print(self.__species, ", ", self.__hp, "hp", ", Types: ",
              ", ".join(self.__types), sep="")
        print()

    def heal(self, amount):
        if type(amount) is not int:
            return False
        if int(amount) >= 0:
            if amount + self.__hp > self.__max_hp:
                print("{1} was healed for {0} hp.".format(
                    self.__max_hp- self.__hp,
                self.__species))
                self.__hp = self.__max_hp
            else:
                self.__hp += amount
                print("{0} was healed for {1} hp.".format(self.__species, amount))
            return True
        else:
            return False

    def damage(self, amount):
        if type(amount) is not int:
            return False
        if int(amount >= 0):
            if self.__hp - amount <= 0:
                print("{0} lost {1} hp".format(self.__species, self.__hp))
                self.__hp = 0
                print("{} fainted!".format(self.__species))
            else:
                print("{0} lost {1} hp".format(self.__species, amount))
                self.__hp -= amount
            return True
        else:
            return False

    def add_type(self, type):
        type = type[0].upper() + type[1:].lower()
        if type in TYPES.keys():
            self.__types.append(type)
            return True
        else:
            return False


    def set_types(self, newTypes = []):
        newTypes2 = []
        for f in newTypes:
            f = f[0].upper() + f[1:].lower()
            newTypes2.append(f)
            if f not in TYPES.keys():
                return False
            self.__types = newTypes2
            return True

    def add_move(self, name, power, type):
        if name.count(" ") > 1:
            return False
        elif name.count(" ") == 1:
            n = name.index(" ")
            name = name.lower()
            name = name[0].upper()+ name[1:n].lower() +\
                   name[n+1].upper()+name[n+2:].lower()
        else:
            name = name[0].upper + name[1:].lower()
        array = []
        array.append(name)
        array.append(power)
        array.append(type)
        self.__moves.append(array)
        return True

    def move_info(self):
        ()


def main():
    mew = Pokemon("Mew", ["Psychic"], 100, 40)
    print(mew.info())
    print(mew.add_move("Pound", 40, "Normal"))
    mew.move_info()

main()