"""
TIE-02100 Johdatus ohjelmointiin
Tehtävän 14.1. ratkaisu
Mikael Yli-Mattila, mikael.yli-mattila@student.tut.fi, opiskelijanumero: 246268
"""


class Router:
    def __init__(self, name):
        self.name = name
        self.neighbours = []
        self.networks = {}

    def print_info(self):
        print("  " + self.name)
        print("    N:", ", ".join(sorted(self.neighbours)))
        print("    R: ", end="")
        array = []
        for network, distance in sorted(self.networks.items()):
            array.append("{0}:{1}".format(network, distance))
        print(", ".join(array))

    def add_neighbour(self, neighbour):
        self.neighbours.append(neighbour.name)

    def add_network(self, network, distance):
        self.networks[network] = int(distance)

    def receive_routing_table(self, router):
        for network, distance in router.networks.items():
            if network in self.networks.keys():
                if self.networks[network] > distance + 1:
                    self.networks[network] = distance + 1
            else:
                self.networks[network] = distance + 1

    def has_route(self, network):
        if network not in self.networks.keys():
            print("Route to the network is unknown.")
        else:
            if self.networks[network] == 0:
                print("Router is an edge router for the network.")
            else:
                print("Network {0} is {1} hops away".format(
                    network, self.networks[network]))


def read_input_file(router_file):
    # Returns dictionary of routers and information
    # if an error occurred while reading the file
    routers = {}
    if len(router_file) > 0:
        try:
            file = open(router_file, "r")
        except:
            return None, True
        for line in file:
            if line.count("!") != 2:
                return None, True
            array = line.strip().split("!")
            if len(array[0]) == 0:
                return None, True
            router = Router(array[0])
            if len(array[1]) > 0:
                for neighbour in array[1].split(";"):
                    router.add_neighbour(Router(neighbour))
            if len(array[2]) > 0:
                for network in array[2].split(";"):
                    router.add_network(network.split(":")[0],
                                        network.split(":")[1])
            routers[array[0]] = router
    return routers, False


def main():
    router_file = input("Network file: ")
    routers, error_occurred = read_input_file(router_file)
    if error_occurred:
        print("Error: the file could not be read or there is something "
              "wrong with it.")
        return
    while True:
        command = input("> ")
        command = command.upper()

        if command == "P":
            router = str(input("Enter router name: "))
            if router in routers.keys():
                routers[router].print_info()
            else:
                print("Router was not found.")

        elif command == "PA":
            for router in sorted(routers.keys()):
                routers[router].print_info()

        elif command == "S":
            router = str(input("Sending router: "))
            for neighbour in routers[router].neighbours:
                routers[neighbour].receive_routing_table(routers[router])

        elif command == "C":
            router1 = str(input("Enter 1st router: "))
            router2 = str(input("Enter 2nd router: "))
            routers[router1].add_neighbour(routers[router2])
            routers[router2].add_neighbour(routers[router1])

        elif command == "RR":
            router = str(input("Enter router name: "))
            network = str(input("Enter network name: "))
            routers[router].has_route(network)

        elif command == "NR":
            router_to_add = str(input("Enter a new name: "))
            if router_to_add not in routers.keys():
                routers[router_to_add] = Router(router_to_add)
            else:
                print("Name is taken.")

        elif command == "NN":
            router = str(input("Enter router name: "))
            network = str(input("Enter network: "))
            distance = str(input("Enter distance: "))
            routers[router].add_network(network, distance)

        elif command == "Q":
            print("Simulator closes.")
            return

        else:
            print("Erroneous command!")
            print("Enter one of these commands:")
            print("NR (new router)")
            print("P (print)")
            print("C (connect)")
            print("NN (new network)")
            print("PA (print all)")
            print("S (send routing tables)")
            print("RR (route request)")
            print("Q (quit)")


main()
