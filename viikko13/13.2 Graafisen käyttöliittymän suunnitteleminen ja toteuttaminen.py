
from tkinter import *

class MainWindow:
    def __init__(self):
        self.__main_window = Tk()
        self.__main_window.geometry("350x150")
        self.__main_window.title("Length Unit Converter")

        Grid.rowconfigure(self.__main_window, 2, weight=1)
        Grid.rowconfigure(self.__main_window, 4, weight=1)

        self.__label_value = Label(self.__main_window,text="Value:")
        self.__label_value.grid(row=0, column=0)

        self.__label_error = Label(self.__main_window, text="")
        self.__label_error.grid(row=0, column=2)

        self.__textbox_value = Entry(self.__main_window, width="35", bg="white")
        self.__textbox_value.grid(row=0, column=1)

        self.__label_result = Label(self.__main_window, text="Result:")
        self.__label_result.grid(row=2, column=0)

        self.__textbox_result = Entry(self.__main_window, width="35")
        self.__textbox_result.grid(row=2, column=1)

        self.__label_unit_from = Label(self.__main_window,text="Unit from:")
        self.__label_unit_from.grid(row=1, column=0)

        self.__label_unit_to = Label(self.__main_window,text="Unit to:")
        self.__label_unit_to.grid(row=1, column=1)

        unit_from = StringVar(self.__main_window)
        unit_from.set("mm")
        self.__unit_from_dropdown_list = OptionMenu(
            self.__main_window, unit_from,  "mm", "cm", "inch", "foot", "m")
        self.__unit_from_dropdown_list.grid(row=1, column=1, sticky="w")

        unit_to = StringVar(self.__main_window)
        unit_to.set("mm")
        self.__unit_to_dropdown_list = OptionMenu(
            self.__main_window, unit_to, "mm", "cm", "inch", "foot", "m")

        self.__unit_to_dropdown_list.grid(row=1, column=1, sticky="e")

        self.__convert_button = Button(self.__main_window, text="Convert",
                command=lambda: self.convert(unit_from.get(), unit_to.get()))

        self.__convert_button.grid(row=3, column=1, sticky="nsew")

        self.__quit_button = Button(self.__main_window, text="Quit",
                                    command=self.quit)
        self.__quit_button.grid(row=5, column=1, sticky="nsew")

        self.__main_window.mainloop()

    def convert(self, unit_from, unit_to):
        units = {
            "mm": {"cm": 0.1, "inch": 0.03937, "foot": 0.00328, "m": 0.001},
            "cm": {"mm": 10, "inch": 0.3937, "foot": 0.0328, "m": 0.01},
            "inch": {"mm": 25.4, "cm": 2.54, "foot": 0.0833, "m": 0.0254},
            "foot": {"mm": 304.8, "cm": 30.48, "inch": 12, "m": 0.3048},
            "m": {"mm": 1000, "cm": 100, "inch": 39.37, "foot": 3.2808}
        }
        self.__textbox_result.delete(0, END)
        try:
            value = float(self.__textbox_value.get())
            self.__label_error.config(text="")
            self.__textbox_value.config(bg="white")
        except:
            self.__label_error.config(text="Invalid value")
            self.__textbox_value.config(bg="pink")
            return

        if unit_from == unit_to:
            self.__textbox_result.insert(0, value)
        else:
            result = float(value) * units[unit_from][unit_to]
            self.__textbox_result.insert(0, result)

    def quit(self):
        self.__main_window.destroy()


def main():
    interface = MainWindow()

main()


