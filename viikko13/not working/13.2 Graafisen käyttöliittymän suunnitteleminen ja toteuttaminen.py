

from tkinter import *
import random

class MainWindow:
    def __init__(self):
        self.__main_window = Tk()
        self.__main_window.geometry("400x150")
        self.__main_window.title("Memory Game")

        #self.__main_window.overrideredirect(1)
        #self.__main_window.wm_attributes('-fullscreen','true')

        Grid.rowconfigure(self.__main_window, 2, weight=1)
        Grid.rowconfigure(self.__main_window, 4, weight=1)

        self.__label_player_name = Label(self.__main_window,text="Player name")
        self.__label_player_name.grid(row=0, column=0)

        self.__textbox_player_name = Text(self.__main_window, height=1, width="35")
        self.__textbox_player_name.grid(row=0, column=1)

        self.__label_difficulty = Label(self.__main_window,text="Difficulty level:")
        self.__label_difficulty.grid(row=1, column=0)

        difficulty_level = StringVar(self.__main_window)
        difficulty_level.set("Easy")
        self.__dificulty_dropdown_list = OptionMenu(self.__main_window, difficulty_level, "Easy", "Medium", "Hard")
        self.__dificulty_dropdown_list.grid(row=1, column=1, sticky="w")

        self.__start_game_button = Button(self.__main_window, text="Start game", command=lambda: self.start(difficulty_level.get()))
        self.__start_game_button.grid(row=3, column=1, sticky="nsew")

        self.__quit_game_button = Button(self.__main_window, text="Quit game", command=self.quit)
        self.__quit_game_button.grid(row=5, column=1, sticky="nsew")

        self.__main_window.mainloop()

    def start(self, difficulty):
        self.__main_window.withdraw()
        #__main_window.deiconify() to visible
        game = GameWindow(difficulty)

    def quit(self):
        self.__main_window.destroy()


class GameWindow:
    def __init__(self, difficulty):
        self.__game_window = Tk()
        #self.__game_window.geometry("400x150")
        self.__game_window.title("Memory Game " + difficulty)
        self.__button_list = []

        if difficulty == "Easy":
            values = ["A", "A", "B", "B"]
            for r in range(2):
                for c in range(2):
                    index = random.randrange(0,len(values))
                    value = values[index]
                    del values[index]
                    button = Button(self.__game_window, text="", height=5, width=10,
                                  borderwidth=2, command=lambda: self.button_click(r,c, button)).grid(row=r, column=c)
                    self.__button_list.append(GameButton(r,c,value, button))


            self.__game_window.mainloop()

    def button_click(self, row, column, button):
        #button.configure(text="1234")
        #button["text"] = "ad"
        print(row)


    def end_game(self):
        pass

class GameButton:
    def __init__(self, row, column, value, button):
        self.row = row
        self.column = column
        self.value = value
        self.button = button


def main2():
    root = Tk()
    root.title("Memory game")

    button = Button(root, text="QUIT")
    #, height= 50, width=50)
    button.grid(row=3, column=1, columnspan=2, sticky="S")
    button.pack()
    root.mainloop()

def main():
    interface = MainWindow()

main()


