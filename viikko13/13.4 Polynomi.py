
class Polynomial:
    def __init__(self):
        self.items_by_exponents = {}

    def add_item(self, factor, exponent):
        if exponent not in self.items_by_exponents.keys():
            self.items_by_exponents[exponent] = factor
        else:
            self.items_by_exponents[exponent] += factor

    def get_copy(self):
        copy = Polynomial()
        for exponent, factor in self.items_by_exponents.items():
            copy.add_item(factor, exponent)
        return copy

    def __str__(self):
        for key, value in list(self.items_by_exponents.items()):
            if value == 0:
                del self.items_by_exponents[key]
        if not bool(self.items_by_exponents):
            return str(0)
        return " + ".join("{0}x^{1}".format(f, e) for e, f in sorted(self.items_by_exponents.items(), reverse=True))

    def __add__(self, other):
        result = self.get_copy()
        for exponent, factor in other.items_by_exponents.items():
            if exponent in result.items_by_exponents.keys():
                result.items_by_exponents[exponent] += factor
            else:
                result.items_by_exponents[exponent] = factor
        return result

    def __sub__(self, other):
        result = self.get_copy()
        for exponent, factor in other.items_by_exponents.items():
            if exponent in result.items_by_exponents.keys():
                result.items_by_exponents[exponent] -= factor
            else:
                result.items_by_exponents[exponent] = -factor
        return result

    def __mul__(self, other):
        result = Polynomial()
        for exponent, factor in list(self.items_by_exponents.items()):
            for exponent2, factor2 in list(other.items_by_exponents.items()):
                result.add_item(factor * factor2, exponent + exponent2)
        return result


def main():
    polynomials, error_occurred = read_file()
    if error_occurred:
        return
    end = False
    while end is False:
        end = read_command(polynomials)


def read_command(polynomials):
    # Returns value if quit command is given
    valid_operators = ["+", "-", "*"]
    command = input(">")
    if command == "quit":
        print("Bye bye!")
        return True
    items = command.split(" ")
    try:
        polynomial1 = int(items[0])
        polynomial2 = int(items[2])
    except:
        print("Error: entry format is memory_location operation memory_location.")
        return False
    if len(items) != 3:
        print("Error: entry format is memory_location operation memory_location.")
    elif polynomial1 > len(polynomials) or polynomial2 > len(polynomials):
        print("Error: the given memory location does not exist.")
    elif items[1] not in valid_operators:
        print("Error: unknown operator.")
    else:
        print("Memory location {0}: {1}".format(polynomial1, polynomials[polynomial1]))
        print("Memory location {0}: {1}".format(polynomial2, polynomials[polynomial2]))
        print("The simplified result:")
        if items[1] == "+":
            print(polynomials[polynomial1] + polynomials[polynomial2])
        elif items[1] == "-":
            print(polynomials[polynomial1] - polynomials[polynomial2])
        else:
            print(polynomials[polynomial1] * polynomials[polynomial2])
    return False


def read_file():
    #  Returns the list of polynomials and if an error occurred while reading
    # the file
    polynomials = []
    file_name = input("Enter file name: ")
    try:
        file = open(file_name, "r")
    except:
        print("Error in reading the file.")
        return None, True
    for line in file:
        polynomial = Polynomial()
        for item in line.strip().split(";"):
            polynomial.add_item(int(item.split(" ")[0]), int(item.split(" ")[1]))
        polynomials.append(polynomial)

    return polynomials, False


main()