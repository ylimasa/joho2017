raha = [10, 5, 2, 1]
maara = [0]*4
nimi = ["ten-euro notes", "five-euro notes",
        "two-euro coins", "one-euro coins"]

def main():
    hinta0 = input("Purchase price: ")
    hinta = int(hinta0)
    annetaan0 = input("Paid amount of money: ")
    annetaan = int(annetaan0)
    if hinta != annetaan:
        print("Offer change:")
    else:
        print("No change")
    takas = annetaan - hinta
    for k in range(0,4):
        maara[k] = (takas // raha[k])
        takas = takas - maara[k] * raha[k]
        if maara[k] != 0:
            print(maara[k], nimi[k])

main()