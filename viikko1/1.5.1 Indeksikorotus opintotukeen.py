studyBenefits = float(input("Enter the amount of the study benefits: "))
print("If the index raise is 1.17 percent, the study benefit,")
print("after a raise, would be {0} euros".format(studyBenefits * 1.0117))

print("and if there was another index raise, the study")
print("benefits would be as much as {0} euros"
      .format(studyBenefits * 1.0117 * 1.0117))