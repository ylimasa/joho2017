v = ["R", "P", "S", "R"]

def main():
    p1 = input("Player 1, enter your choice (R/P/S): ")
    p2 = input("Player 2, enter your choice (R/P/S): ")
    if p1 == p2:
        print("It's a tie!")
    else:
        if v[v.index(p1)+1] == p2:
            print("Player 2 won!")
        else:
            print("Player 1 won!")

main()
