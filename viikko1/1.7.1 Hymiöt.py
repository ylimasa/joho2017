hu7 = ":-|"
h89 = ":-)"
hu4 = ":-("
h1 = ":'("
h10 = ":-D"

s = "A suitable smiley would be "
def main():
    f = input("How do you feel? (1-10) ")
    try:
        n = int(f)
    except:
        print("Bad input!")
        return

    if n>10 or n<1:
        print("Bad input!")
    elif n == 1:
        print(s+h1)
    elif n == 10:
        print(s+h10)
    elif n>7:
        print(s + h89)
    elif n>3:
        print(s + hu7)
    else:
        print(s + hu4)

main()