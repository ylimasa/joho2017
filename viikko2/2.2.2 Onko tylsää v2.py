
letters = ["y", "Y", "n", "N"]


def main():

    answer = input("Answer Y or N: ")
    if not answer in letters:
        while answer not in letters:
            print("Incorrect entry.")
            answer = input("Please retry: ")
        print("You answered {0}".format(answer))

    else:
        print("You answered {0}".format(answer))

main()
