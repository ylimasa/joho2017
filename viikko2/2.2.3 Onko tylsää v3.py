
letters = ["y", "n"]


def main():

    answer = input("Bored? (y/n) ")

    while answer != "y":
        if answer not in letters:
            print("Incorrect entry.")
            answer = input("Please retry: ")
        else:
            answer = input("Bored? (y/n) ")


    print("Well, let's stop this, then.")


main()
