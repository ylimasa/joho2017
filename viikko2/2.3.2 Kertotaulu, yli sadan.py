

def main():
    n = int(input("Choose a number: "))
    for f in range(1,102):
        print("{0} * {1} = {2}".format(f, n, f*n))
        if f*n > 100:
            return

main()