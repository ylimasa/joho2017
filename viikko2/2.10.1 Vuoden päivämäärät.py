months = [31,28,31,30,31,30,31,31,30,31,30,31]

def main():
    for month in range(0,12):
        for day in range(1,months[month]+1):
            print("{0}.{1}.".format(day,month+1))


main()