months = [31,28,31,30,31,30,31,31,30,31,30,31]

def main():
    c = 7
    for month in range(0,12):
        for day in range(1,months[month]+1):
            if not (month == 0 and (day == 1 or day == 2)):
                if c == 7:
                    print(day,month+1,sep=".",end=".\n")
                    c=1
                else:
                    c+=1

main()