
def main():
    bored = False
    while not bored:
        bored = bool(input("Bored? (y/n) ") != "n")
        if bored:
            print("Well, let's stop this, then.")

main()
